Name:           nzbhydra2
Version:        7.12.3
Release:        1%{?dist}

Summary:        Meta search for newznab indexers and torznab trackers
License:        Apache License 2.0
URL:            https://github.com/theotherp/nzbhydra2

Source0: https://github.com/theotherp/nzbhydra2/archive/refs/tags/v%{version}.tar.gz
Source1: nzbhydra2.env
Source2: nzbhydra2.service
# https://aur.archlinux.org/cgit/aur.git/tree/?h=nzbhydra2
Patch0: disable-update-checks.patch
Patch1: skip-unneeded-builds.patch


BuildArch: noarch

BuildRequires: java-17-openjdk-devel
BuildRequires: maven
BuildRequires: systemd-rpm-macros


Requires: python3
Requires: java-17-openjdk-headless

%description
Meta search for newznab indexers and torznab trackers


%prep
%setup -q -n nzbhydra2-%{version}
patch -p1 < %{PATCH0}
patch -p1 < %{PATCH1}


%build
export JAVA_HOME=/usr/lib/jvm/java-17
mvn -Dmaven.test.skip -pl core -am clean package


%check
mvn -pl core -am test


%install
install -D --mode=0755 other/wrapper/nzbhydra2wrapperPy3.py %{buildroot}%{_datadir}/nzbhydra2/nzbhydra2wrapperPy3.py
install -D --mode=0644 core/target/core-%{version}-exec.jar %{buildroot}%{_datadir}/java/nzbhydra2/core-%{version}-exec.jar
install -D --directory --mode=0700 %{buildroot}%{_sharedstatedir}/nzbhydra2
%{__mkdir} --parents %{buildroot}%{_sysconfdir}/sysconfig
sed \
  -e 's|{_datadir}|%{_datadir}|g' \
  %{SOURCE1} > %{buildroot}%{_sysconfdir}/sysconfig/nzbhydra2
chmod 0644 %{buildroot}%{_sysconfdir}/sysconfig/nzbhydra2
%{__mkdir} --parents %{buildroot}%{_unitdir}
sed \
  -e 's|{_datadir}|%{_datadir}|g' \
  -e 's|{_sharedstatedir}|%{_sharedstatedir}|g' \
  -e 's|{_sysconfdir}|%{_sysconfdir}|g' \
  %{SOURCE2} > %{buildroot}%{_unitdir}/nzbhydra2.service
chmod 0644 %{buildroot}%{_unitdir}/nzbhydra2.service


%files
%doc readme.md
%license LICENSE
%defattr(-, root, root, -)
%{_datadir}/nzbhydra2
%{_datadir}/java/nzbhydra2
%{_sysconfdir}/sysconfig/nzbhydra2
%{_unitdir}/nzbhydra2.service
%defattr(-, nzbhydra2, nzbhydra2, -)
%{_sharedstatedir}/nzbhydra2

%clean
rm -rf %{buildroot}


%pre
getent group nzbhydra2 > /dev/null || groupadd --system nzbhydra2
getent passwd nzbhydra2 > /dev/null || \
  useradd --system --home-dir %{_sharedstatedir}/nzbhydra2 --gid nzbhydra2 \
  -s /sbin/nologin -c "nzbhydra2 daemon" nzbhydra2
exit 0


%post
%systemd_post nzbhydra2.service


%preun
%systemd_preun nzbhydra2.service


%postun
%systemd_postun nzbhydra2.service


%changelog
%autochangelog
